/*!
 * controller home
 *   
 *  @package     controllers/service.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        controllers/service
 *  @since       0.0.1
 */


module.exports = (app) => {
 	var controller = {};
 	var Service = app.models.service;

 	controller.list = (req, res) => {
 		var promisse = Service.find();
 		promisse.then(
 			(services) => {
	 			res.status(200).json(services);
	 		},
	 		(err) => {
	 			res.status(500).json(err);
	 	});
 	};

 	controller.find = (req, res) => {
 		
 		var id = req.params.id;
 		
 		var promisse = Service.findById(id);
 		
 		promisse.then(
 			(service) => {
	 			if(!service) throw new Error('service not found');
	 			res.status(200).json(service);
	 		},
	 		(err) => {
	 			res.send(404, err);
	 	});
 		
 	};

 	controller.remove = (req, res) => {

 		var id = req.params.id;
 		var query = {'_id': id};
 		var promisse = Service.remove(query);

 		promisse.then( 
 			() => {
 				res.send(204);
 			},
 			(err) => {
 				res.send(500).json(err);
 			}
 		);
 	}

 	controller.save = (req, res) => {

 		var id = req.params.id;

 		if(id) {
 			var promisse = Service.findByIdAndUpdate({_id: id}, req.body);
 			promisse.then( 
	 			(service) => {
	 				res.json(service);
	 			},
	 			(err) => {
	 				res.status(500).json(err);
	 			}
 			);
 		} else {
 			var promisse = Service.create(req.body);
 			promisse.then( 
	 			(service) => {
	 				res.status(201).json(service);
	 			},
	 			(err) => {
	 				res.status(500).json(err);
	 			}
 			);
 		}

 	}

 	controller.verify = (req, res) => {

 		var promisse = Service.find();

 		promisse.then(
 			(services) => {
	 			res.json({message: 'Verificando'});
	 			app.services.monitor.verify(services);
	 		},
	 		(err) => {
	 			res.status(500).json(err);
	 	});

 	}

	controller.ping = (req, res) => {

 		var id = req.params.id;

 		if(!id) res.status(404).json({message: 'servico nao encontrado'});

 		var promisse = Service.findByIdAndUpdate({_id: id}, {  config: { last_ping: Date.now() } });

		promisse.then( 
			(service) => {
				res.json(service);
			},
			(err) => {
				res.status(500).json(err);
			}
		);

 	}

 	return controller;
 }