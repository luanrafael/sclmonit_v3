var request = require('request');

module.exports = () => {

	var monitor = {};

	monitor.verify = (register, callback) => {

		var token = '#TOVIVO#__' + Date.now(); 

		var post_like_client = {

			idRmte: 'Mdk-#SCL_MONITOR#',
			idConsumidor: 'Avulso',
			idDest: 'Owner-#SCL_MONITOR#',
			msgEnv: '',
			msgResp: '100',
			msgRet: '',
		}

		var post_like_server = {

			idRmte: 'WEB-#SCL_MONITOR#',
			idConsumidor: 'Avulso',
			idDest: 'BDJ-#SCL_MONITOR#',
			msgEnv: 'm='+ token + '~~10',
			msgResp: '100',
			msgRet: 'false',
		}

		request.post({ url: register.config, form: post_like_client, timeout: 4500}, function(err, res, body){
			
			if(err){
				callback(err, {id: register.id, error: true });
				return;
			};

			post_like_client.msgEnv = "OK";
	
			var info = JSON.parse(body);

			if(info == token){
				callback(null, {id: register.id, error: false });
			} else {
				callback(null, {id: register.id, error: true });
			}

			request.post({ url: 'http://broker.kanban360.com.br:8090/cloud',form: post_like_client, timeout: 500}, function(err,res, body){
				
			});

		});

		request.post({ url: register.config, form: post_like_server, timeout: 1500}, function(err, res, body){

			if(err){
				callback(err, {id: register.id, error: true });
				return;
			};

			var info = JSON.parse(body);
			
			callback(null, {id: register.id, error: info.msgResp == 'OK' });

		});
	}

	return monitor;

};
