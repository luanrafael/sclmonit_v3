var ftpClient = require('ftp');

module.exports = () => {

	var monitor = {};

	monitor.verify = (register, callback) => {

		console.log('veficando...', register.name);
		
		var client = new ftpClient();

		client.on('ready', function(){
			client.end();
		});

		client.on('error', function(err){
			callback(err, {id: register.id, error: true});
		});


		client.on('end', function(){
			callback(null, {id: register.id, error: false});
		});

		client.connect(register.config);
	}

	return monitor;

};
