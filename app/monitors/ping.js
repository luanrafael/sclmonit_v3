
module.exports = () => {

	var monitor = {};

	monitor.verify = (register, callback) => {

		if(!register.config.last_ping){
			callback(true, {id: register.id, error: true});
			return;
		}

		var last_ping = register.config.last_ping;

		var diff = Date.now() - last_ping;

		var diff_minutes = Math.floor(diff / 1000 / 60);
		console.log("##### DIFF: " + diff_minutes + " #####");
		if(diff_minutes > 10){
			callback(true, {id: register.id, error: true});
		} else {
			callback(null, {id: register.id, error: false});
		}

	}

	return monitor;

};
