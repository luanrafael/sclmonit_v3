
angular.module("scl_monitor").factory("scl_monitor_api", function($http){
  var _getServices = function() {
    return $http.get("/services");
  }

  return {
    getServices: _getServices,
  };

});
