


module.exports = {
	
	env: 'development',
	db: 'mongodb://127.0.0.1:27017/scl_monitor',
	port: '8080',
	address: 'localhost',
	domain: 'localhost'

}