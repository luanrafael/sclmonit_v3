/*!
 *  description
 *   
 *  @package     server.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        path
 *  @since      1.0.0
 */


var http = require('http');
var app = require('./config/express')();
var config = require('./config/config')();

Object.assign=require('object-assign')

require('./config/database')(config.db);

console.log('ADDRESS: ', config.address || '0.0.0.0');
console.log('PORTA: ', config.port || 8080);


app.listen(config.port || 8080, config.address || '0.0.0.0')

console.log('Express server escutando na porta: ', config.port);

module.exports = app ;